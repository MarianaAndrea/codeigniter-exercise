	document.addEventListener("DOMContentLoaded", function(){
		const btn_s = document.getElementById("btn_strships");
		const actionUrl1 = "https://swapi.dev/api/starships/";
		btn_s.addEventListener("click", function(){
			const optionsFetch = {
				method: "GET",
				// body: JSON.stringify({
				// 	val: val
				// }),
				headers: {
					"Content-type": "application/json; charset=UTF-8",
					"X-Requested-With": "XMLHttpRequest"
				}
				};

				return fetch(actionUrl1, optionsFetch)
				.then((response) => response.json())
				.then((data => {
					console.log(data);
				}))
				.catch(error =>{
					console.log(error);
				})
		});

		const btn_v = document.getElementById("btn_vehicles");
		const actionUrl2 = "https://swapi.dev/api/vehicles/";
		btn_v.addEventListener("click", function(){
			const optionsFetch = {
				method: "GET",
				// body: JSON.stringify({
				// 	val: val
				// }),
				headers: {
					"Content-type": "application/json; charset=UTF-8",
					"X-Requested-With": "XMLHttpRequest"
				}
				};

				return fetch(actionUrl2, optionsFetch)
				.then((response) => response.json())
				.then((data => {
					console.log(data);
				}))
				.catch(error =>{
					console.log(error);
				})
		});
	});
