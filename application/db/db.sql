
CREATE DATABASE swapi;

USE swapi;

CREATE TABLE starship (
    name VARCHAR(255), 
    model VARCHAR(255), 
    starship_class VARCHAR(255),
    manufacturer VARCHAR(255),
    cost_in_credits VARCHAR(255),
    length VARCHAR(255),
    crew VARCHAR(255),
    passengers VARCHAR(255),
    max_atmosphering_speed VARCHAR(255),
    hyperdrive_rating VARCHAR(255), -- The class of this starships hyperdrive.
    mglt VARCHAR(255),
    cargo_capacity VARCHAR(255),
    consumables VARCHAR(255),
    -- films VARCHAR(255), 
    -- pilots VARCHAR(255),
    url VARCHAR(255),
    created VARCHAR(255),
    edited VARCHAR(255)
);

CREATE TABLE vehicles (
    name VARCHAR(255), 
    model VARCHAR(255), 
    vehicle_class VARCHAR(255),
    manufacturer VARCHAR(255),
    length VARCHAR(255),
    cost_in_credits VARCHAR(255),
    crew VARCHAR(255),
    passengers VARCHAR(255),
    max_atmosphering_speed VARCHAR(255),
    cargo_capacity VARCHAR(255), -- The class of this starships hyperdrive.
    consumables VARCHAR(255),
    -- films VARCHAR(255), 
    -- pilots VARCHAR(255),    
    url VARCHAR(255),
    created VARCHAR(255),
    edited VARCHAR(255)
);

--Datos mokeados para vehiculos
INSERT INTO vehicles (
    name, 
    model, 
    vehicle_class,
    manufacturer,
    length,
    cost_in_credits,
    crew,
    passengers,
    max_atmosphering_speed,
    cargo_capacity, -- The class of this starships hyperdrive.
    consumables,
    -- films, 
    -- pilots,    
    url,
    created,
    edited
    ) 
    VALUES(
        'Sand',
        'Digger',
        'COrrellia',
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        -- NULL,
        -- NULL,
        NULL,
        NULL,
        NULL
    );