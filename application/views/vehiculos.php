<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Welcome to SWAPI challenge</title>

	<style type="text/css">

	::selection { background-color: #E13300; color: white; }
	::-moz-selection { background-color: #E13300; color: white; }

	body {
		background-color: #fff;
		margin: 40px;
		font: 13px/20px normal Helvetica, Arial, sans-serif;
		color: #4F5155;
	}

	a {
		color: #003399;
		background-color: transparent;
		font-weight: normal;
		text-decoration: none;
	}

	a:hover {
		color: #97310e;
	}

	h1 {
		color: #444;
		background-color: transparent;
		border-bottom: 1px solid #D0D0D0;
		font-size: 19px;
		font-weight: normal;
		margin: 0 0 14px 0;
		padding: 14px 15px 10px 15px;
	}

	code {
		font-family: Consolas, Monaco, Courier New, Courier, monospace;
		font-size: 12px;
		background-color: #f9f9f9;
		border: 1px solid #D0D0D0;
		color: #002166;
		display: block;
		margin: 14px 0 14px 0;
		padding: 12px 10px 12px 10px;
	}

	#body {
		margin: 0 15px 0 15px;
		min-height: 96px;
	}

	p {
		margin: 0 0 10px;
		padding:0;
	}

	p.footer {
		text-align: right;
		font-size: 11px;
		border-top: 1px solid #D0D0D0;
		line-height: 32px;
		padding: 0 10px 0 10px;
		margin: 20px 0 0 0;
	}

	#container {
		margin: 10px;
		border: 1px solid #D0D0D0;
		box-shadow: 0 0 8px #D0D0D0;
	}
	</style>
</head>
<body>

<div id="container">
	<h1>Welcome</h1>

	<div id="body">
		<h1>SWAPI Lista de Vehículos de la Base</h1>
	</div>

	<table class="table">
		<thead>
			<tr>
			<th scope="col">#</th>
			<th scope="col">Name</th>
			<th scope="col">Model</th>
			<th scope="col">Vehicle Class</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($vehiculos as $vehiculo){ ?>
			<tr>
			<th scope="row"></th>
			<td><?php echo $vehiculo->name; ?></td>
			<td><?php echo $vehiculo->model; ?></td>
			<td><?php echo $vehiculo->vehicle_class; ?></td>
			</tr>
			<?php }?>
			<tr>
			<th scope="row">2</th>
			<td>Jacob</td>
			<td>Thornton</td>
			<td>@fat</td>
			</tr>
			<tr>
			<th scope="row">3</th>
			<td>Larry</td>
			<td>the Bird</td>
			<td>@twitter</td>
			</tr>
		</tbody>
	</table>

	<div>
		<!-- <button id="btn_strships">Ver Starships</button> -->
		<button id="btn_vehicles">Actualizar Vehicles</button>
		<a href="<?php echo site_url('listado')?>">Ver</a>
	</div>

</div>
<?php //print_r(base_url()); exit; ?>
<link rel="stylesheet" href="<?php echo base_url();?>vendor/css/bootstrap.min.css" crossorigin="anonymous">
<script src="<?php echo base_url();?>vendor/code.jquery.com_jquery-3.7.0.min.js" crossorigin="anonymous"></script>
<script src="<?php echo base_url();?>vendor/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
<script type="text/javascript">
	document.addEventListener("DOMContentLoaded", function(){

		const btn_v = document.getElementById("btn_vehicles");
		const actionUrl2 = "https://swapi.dev/api/vehicles/";
		btn_v.addEventListener("click", function(){
			const optionsFetch = {
				method: "GET",
				headers: {
					"Content-type": "application/json; charset=UTF-8",
					"X-Requested-With": "XMLHttpRequest"
				}
				};

				return fetch(actionUrl2, optionsFetch)
				.then((response) => response.json())
				.then((data => {
					console.log(data);
				}))
				.catch(error =>{
					console.log(error);
				})
		});
	});
</script>

</body>
</html>
