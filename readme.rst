###################
 CodeIgniter exercise
###################

Technologies used:
###################
* Codeigniter 3.1
* Bootstrap 4.6 
* Javascript
* Jquery 3.5
* Mysql
* PHP

functionality:
* get and list data from de database
* get data from Star Wars API

Coming:
* update database getting data from star wars API
